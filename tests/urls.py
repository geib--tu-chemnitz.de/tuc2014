from django.conf.urls import url
from django.contrib import admin
from .views import index

urlpatterns = [
    url(r'^$', index),
    url('admin/', admin.site.urls),
]


handler403 = 'tuc2014.views.handle_403'
handler404 = 'tuc2014.views.handle_404'
