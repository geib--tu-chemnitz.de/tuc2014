#!/usr/bin/env python

from setuptools import setup

setup(name='django-tuc2014',
    version='1.0',
    description='Adoption of the TUC Corporate Design for Django',
    author='Daniel Klaffenbach',
    author_email='daniel.klaffenbach@hrz.tu-chemnitz.de',
    url='https://gitlab.hrz.tu-chemnitz.de/urz-django/tuc2014/',
    packages=['tuc2014'],
    package_data = {
        'tuc2014': [
            'locale/de/LC_MESSAGES/django*',
            'static/tuc2014/css/*.css',
            'templates/admin/*.html',
            'templates/tuc2014/*.html',
            'templates/tuc2014/forms/layout/*.html',
            'templates/tuc2014/js/jquery-datepicker-locale/*.js',
        ]
    },
    install_requires=[
        'django>1.5',
    ]
)
