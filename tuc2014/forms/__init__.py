"""
Form base classes which can be used with tuc2014, crispy_forms and bootstrap.

@author: klada
"""
from django import forms
from django.utils.translation import ugettext as _, ugettext_lazy
from crispy_forms.bootstrap import FormActions, FieldWithButtons
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Div, Layout, Submit

from tuc2014._forms import ChangeLanguageForm as _BaseChangeLanguageForm


class TucFormMixin(object):
    """
    Adds additional attributes to the form.
    """
    # May be used to specify a crispy_forms layout, without having to overload the constructor
    layout = None
    # Set this to True to automatically set the layout, including a submit button
    crispy_default_layout = True

    def add_aria_attributes(self):
        """
        Adds ARIA attributes to form widgets.
    
        @see: http://www.w3.org/WAI/intro/aria
        """
        for field in self.fields:
            if self.fields[field].required:
                self.fields[field].widget.attrs['aria-required'] = 'true'
            else:
                self.fields[field].widget.attrs['aria-required'] = 'false'

            # If a field has a help_text, link it to the field through ARIA attribute
            if self.fields[field].help_text:
                self.fields[field].widget.attrs['aria-describedby'] = 'hint_id_{}'.format(field)

    def add_crispy_attributes(self):
        """
        Initializes the crispy_forms FormHeler() with default values.
        
        @see: http://django-crispy-forms.readthedocs.org/en/latest/form_helper.html
        """
        self.helper = FormHelper(self)
        # self.helper.html5_required = True
        self.helper.form_method = 'post'
        self.helper.form_action = '.'
        self.helper.form_class = 'form-horizontal tuc-form'
        self.helper.label_class = 'col-xm-3'
        self.helper.field_class = 'col-xm-9'
        
        if isinstance(self.layout, Layout):
            self.helper.layout = self.layout
        elif self.crispy_default_layout:
            self.helper.layout = Layout(
                Div(*self.fields.keys()),
                FormActions(
                    Submit('submit', _('Submit'))
                )
            )


class TucForm(forms.Form, TucFormMixin):
    """
    Base class for all forms.
    """
    def __init__(self, *args, **kwargs):
        super(TucForm, self).__init__(*args, **kwargs)
        self.add_aria_attributes()
        self.add_crispy_attributes()


class TucModelForm(forms.ModelForm, TucFormMixin):
    """
    Base class for all model forms.
    """
    def __init__(self, *args, **kwargs):
        super(TucModelForm, self).__init__(*args, **kwargs)
        self.add_aria_attributes()
        self.add_crispy_attributes()


class ChangeLanguageForm(_BaseChangeLanguageForm, TucForm):
    """
    Form for selecting the main language.
    """
    layout = Layout(
        'next',
        FieldWithButtons('language', Submit('submit', ugettext_lazy('Submit')))
    )

    def __init__(self, redirect_to, *args, **kwargs):
        super(ChangeLanguageForm, self).__init__(*args, **kwargs)
        self.helper.form_action = self.form_action
        self.add_aria_attributes()
        self.add_crispy_attributes()
