# -*- coding: utf-8 -*-
from __future__ import unicode_literals
"""
Template-Tag library for JavaScript-related functions.

@author: klada
"""
from django import template
from django.template import loader, TemplateDoesNotExist
from django.utils.safestring import mark_safe
from django.utils.translation import get_language

register = template.Library()

@register.simple_tag
def datefield_locale_include():
    """
    Includes the datepicker locale definition for the current locale.
    
    @return: The necessary JavaScript code
    @rtype: string
    """
    lang = get_language()
    if lang == "en" or lang == 'en-US':
        return "datefield_datepicker_init = true;"
    
    # The datepicker requires uppercase variants, such as zh-CN, Django only uses lowercase variants
    lang = lang.lower() 
    if '-' in lang:
        base, variant = lang.split('-')
        variant = variant.upper()
    else:
        base = lang
        variant = lang.upper()
    
    try_template_names = (
        'tuc2014/js/jquery-datepicker-locale/datepicker-%s-%s.min.js' %(base, variant),
        'tuc2014/js/jquery-datepicker-locale/datepicker-%s.min.js' %base,
    )
    
    for name in try_template_names:
        try:
            content = loader.render_to_string(name)
        except TemplateDoesNotExist:
            pass
        else:
            return mark_safe(content + ' datefield_datepicker_init = true;')
    return ""