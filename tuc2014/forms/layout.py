"""
Fields and Layout classes which can be used with crispy-forms.

@author: klada
@see: http://django-crispy-forms.readthedocs.org/en/latest/layouts.html
"""
import json
from crispy_forms.layout import Field
from django.utils.safestring import mark_safe

class DateField(Field):
    """
    Crispy forms layout field which adds a jquery-ui datepicker
    to the widget.
    
    The datepicker is fully customizable through keyword arguments, but
    the arguments need to be prefixed with `datepicker_`.
    
    Example:
    
        DateField('my_date_field', datepicker_maxDate='+1M', datepicker_minDate='0') 
    """
    template = 'tuc2014/forms/layout/datefield.html'
    
    def __init__(self, *args, **kwargs):
        # get datepicker arguments from kwargs
        self.datepicker_options = {}
        kwargs_keys = kwargs.keys()
        # in python3 kwargs_keys is an interator which raises an exception because 
        # we change the underlying dict. -> converting to list decouples the two
        for i in list(kwargs_keys):
            if i.startswith('datepicker_'):
                arg = kwargs.pop(i)
                #strip the datepicker_prefix
                attr = i.split('datepicker_')[1]
                self.datepicker_options[attr] = arg
        super(DateField, self).__init__(*args, **kwargs)
    
    def render(self, form, form_style, context, *args, **kwargs):
        context['datepicker_options'] = mark_safe(json.dumps(self.datepicker_options))
        return super(DateField, self).render(form, form_style, context, *args, **kwargs)
