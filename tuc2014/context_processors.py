# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import datetime
from django.conf import settings
try:
    # Available in Django 1.10+
    from django.urls import reverse
except ImportError:
    from django.core.urlresolvers import reverse


def tucal(request):

    language_uri = getattr(settings, 'TUCAL_LANGUAGE_SELECT', '')
    if not language_uri:
        try:
            # Try to guess the URL
            language_uri = reverse('tuc2014:change_language')
        except:
            language_uri = ''

    today = datetime.date.today()

    haupttitel = getattr(settings, 'TUCAL_HAUPTTITEL', '')

    return {
        'tucal_autor': getattr(settings, 'TUCAL_AUTOR', 'Webmaster'),
        'tucal_haupttitel': haupttitel,
        'tucal_impressum': getattr(settings, 'TUCAL_IMPRESSUM', '//www.tu-chemnitz.de/tu/impressum.html'),
        'tucal_keywords': getattr(settings, 'TUCAL_KEYWORDS', ''),
        'tucal_language_uri': language_uri,
        'tucal_last_modified': today,
        'tucal_logos': getattr(settings, 'TUCAL_LOGOS', []),
        'tucal_titel': getattr(settings, 'TUCAL_TITEL', haupttitel),
        'tucal_url': request.build_absolute_uri(),
        'tucal_year': "{}".format(today.year),
    }


def press(request):
    return {'display_press': False}
