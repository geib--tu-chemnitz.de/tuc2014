from django.shortcuts import render
from tuc2014 import context_processors

def index(request):
    # Add custom context
    context = context_processors.tucal(request)
    return render(request, 'tests/index.html', context)

