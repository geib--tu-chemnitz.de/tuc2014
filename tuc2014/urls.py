# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf.urls import include, url

from .views import change_language


app_name = 'tuc2014'
urlpatterns = [
    url(r'^i18n/', include(('django.conf.urls.i18n', 'djangoi18n'), namespace='djangoi18n')),
    url(r'^$', change_language, name='change_language')
]
