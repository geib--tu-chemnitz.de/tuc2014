# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms
try:
    # Available in Django 1.10+
    from django.urls import reverse
except ImportError:
    from django.core.urlresolvers import reverse
from django.conf import settings
from django.utils.translation import ugettext_lazy


class ChangeLanguageForm(forms.Form):
    """
    Form for selecting the main language.

    No crispy_forms required.
    """
    next = forms.CharField(
        required=False,
        widget=forms.HiddenInput()
    )
    language = forms.ChoiceField(
        choices=settings.LANGUAGES,
        label=ugettext_lazy('Language'),
    )

    def __init__(self, redirect_to, *args, **kwargs):
        super(ChangeLanguageForm, self).__init__(*args, **kwargs)
        self.form_action = reverse('tuc2014:djangoi18n:set_language')
        self.fields['next'].initial = redirect_to
